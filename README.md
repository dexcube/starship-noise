# Starship Noise Generator
Try it out at dexcube.gitlab.io/starship-noise
## What?
This is a simple application to create custom ambiences reminiscent of ships in the *Star Trek* franchise.

## Why?
I'm planning on playing the *Star Trek Adventures* roleplaying game, and I feel it would be difficult to play the game without some noise reminiscent of the different shows.

## How?
I used the Web Audio API (MDN docs on it [here](https://developer.mozilla.org/en-US/docs/Web/API/Web_Audio_API)). In Javascript, I filled an audio buffer with some white noise, then used 2 [BiquadFilterNodes](https://developer.mozilla.org/en-US/docs/Web/API/BiquadFilterNode), a high pass and a low pass, to approximate the band pass that is typically used to create engine-like sounds, at least among fans.

## License Information
See [LICENSE.md](./LICENSE.md).

Star Trek sounds downloaded from https://www.trekcore.com/audio/