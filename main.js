const startAudio = document.getElementById("startAudio");
startAudio.addEventListener("click", starshipNoise);

const whirPitch = document.getElementById("whirPitch");
const humPitch = document.getElementById("humPitch");

whirPitch.addEventListener("mousemove",starshipNoise);
humPitch.addEventListener("mousemove",starshipNoise);

const whirAmp = document.getElementById("whirAmp");
const humAmp = document.getElementById("humAmp");

whirAmp.addEventListener("mousemove",starshipNoise);
humAmp.addEventListener("mousemove",starshipNoise);

const randomize = document.getElementById("randomize");
randomize.addEventListener("click", randomizeValues);

const beepVol = document.getElementById("beepVol");

const audioContext = new AudioContext();


var noise = undefined;
var noise2 = undefined;

keypressLong();

function starshipNoise() {
    if(noise) {
        noise.stop();
        noise2.stop();
    }
    const buffer = new AudioBuffer({length: 44100*5, sampleRate: 44100});
    const samples = buffer.getChannelData(0);

    for(var i = 0; i < buffer.length; i++) {
        samples[i] = Math.random()*2 - 1;
    }

    noise = new AudioBufferSourceNode(audioContext, {buffer: buffer, loop: true});
    noise2 = new AudioBufferSourceNode(audioContext, {buffer: buffer, loop: true});

    //First noise and effects
    var frequency = whirPitch.value;

    var q = 0;

    //console.log("Frequency: " + frequency + ", Q: " + q);

    const filter = new BiquadFilterNode(audioContext, {type: "lowpass", frequency: frequency, Q:q});

    const filter2 = new BiquadFilterNode(audioContext, {type: "highpass", frequency: frequency - 10, Q: q});

    const gain = new GainNode(audioContext, {gain: whirAmp.value * (400/((frequency)**1.5))});

    noise.connect(filter).connect(filter2).connect(gain).connect(audioContext.destination);
    noise.start();

    //Second noise and effects
    var frequency = humPitch.value;

    var q = 100;

    //console.log("Frequency: " + frequency + ", Q: " + q);

    const filter3 = new BiquadFilterNode(audioContext, {type: "bandpass", frequency: frequency, Q:q});

    const gain2 = new GainNode(audioContext, {gain: humAmp.value * (400/((frequency)**1.5))});

    noise2.connect(filter3).connect(gain2).connect(audioContext.destination);
    noise2.start();

    //Add simple beep noises
}

function keypressLong() {
    var keypressDistance = Math.floor(Math.random()*500) + 200;
    var presses = Math.floor(Math.random()*50);
    var previousTime = 0;
    for(var i = 0; i < presses + 1; i++) {
        var time = keypressDistance*i + Math.round(Math.random()*1000);
        if(time > previousTime + 200) {
            window.setTimeout(keypress, keypressDistance*i + Math.round(Math.random()*1000));
        } else {
            window.setTimeout(keypress, previousTime + keypressDistance);
        }
        previousTime = time;
    }
    window.setTimeout(keypressLong, Math.random()*5000 + presses * keypressDistance);
}
var keysound1;
getBuffer(audioContext, "keyok2.mp3").then((sample) => {keysound1 = sample;});
var keysound2;
getBuffer(audioContext, "keyok3.mp3").then((sample) => {keysound2 = sample;});
function keypress() {
    const keyChoice = Math.round(Math.random());

    const gainFilter = new GainNode(audioContext, {gain: Number(beepVol.value)});

    var buffer;

    if(keyChoice) {
        buffer = keysound1;
    } else {
        buffer = keysound2;
    }

    const sourceNode = new AudioBufferSourceNode(audioContext, {buffer: buffer});

    sourceNode.connect(gainFilter).connect(audioContext.destination);
    sourceNode.start();
}

function randomizeSlider(slider) {
    var value = Math.round(Number(slider.min) + (Number(slider.max) - Number(slider.min))*Math.random());
    slider.value = value;
    console.log(value);
}

function randomizeValues() {
    randomizeSlider(whirPitch);
    randomizeSlider(whirAmp);
    randomizeSlider(humPitch);
    randomizeSlider(humAmp);
    starshipNoise();
}

async function getBuffer(context, file) {
    const audioFile = await fetch(file);
    const audioArray = await audioFile.arrayBuffer();

    return context.decodeAudioData(audioArray);
}